<?php
if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

//require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY . '/Classes/UserFunc/PageNotFoundHandling.php'));

//if(!class_exists('PageNotFoundHandling')){
//    $autoload = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('PageNotFound_Autoloader');
//    $autoload::register();
//}
//
////$sPathToUserFunc = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY . '/Classes/UserFunc/PageNotFoundHandling.php');

//$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = 'USER_FUNCTION:' . $sPathToUserFunc . ':user_pageNotFound->pageNotFound';

/***************
 * Make the extension configuration accessible
 */
if(!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}

$sPathToUserFunc = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . '/Classes/Utility/PageNotFoundHandling.php';
//$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = 'USER_FUNCTION:typo3conf/ext/teufels_thm_error/Classes/Utility/PageNotFoundHandling.php:TEUFELS\TeufelsThmError\Utility\PageNotFoundHandling->pageNotFound';
//$id404 = 'USER_FUNCTION:typo3conf/ext/teufels_thm_error/Classes/Utility/PageNotFoundHandling.php:PageNotFoundHandling->pageNotFound';
//$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = "index.php?id=8";

/***************
 * Reset extConf array to avoid errors
 */
if(is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = serialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}
